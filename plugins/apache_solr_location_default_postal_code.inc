<?php
class apache_solr_location_default_postal_code extends views_plugin_argument_default {
  function argument_form(&$form, &$form_state) {
  }

  function get_argument() {
    $postal_code = 0;
    
    if (TRUE === function_exists('apache_note')) {
      $postal_code = (int) apache_note('GEOIP_POSTAL_CODE');
    }
    
    return $postal_code;
  }
}
