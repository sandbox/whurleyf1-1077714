<?php

class apache_solr_location_handler_sort_distance extends apachesolr_views_handler_sort {

  /**
   * Places the sort into the search parameters.
   */
  public function query() {

    /* These fields have a special "*_sort" field for sorting: */
    $special_sort_fields = array(
      'name' => 'sort_name',
      'title' => 'sort_title',
    );

    $order = strtolower($this->options['order']);
    $field = empty($special_sort_fields[$this->real_field]) ? $this->real_field : $special_sort_fields[$this->real_field];
    $exposed_data = $this->view->exposed_data;
    $handler = $this->view->sort[$exposed_data['sort_by']];

    $postal_code = 0;
    
    // First try to get the postal code from the argument
    foreach ($this->view->argument as $key => $argument) {
      if ($field === $key) {
        $postal_code = $argument->get_value();
        break;
      }
    }
    
    // If that fails but we have an exposed filter use that
    if (TRUE === empty($postal_code) && FALSE === empty($exposed_data[$field])) {
      $postal_code = $exposed_data[$field];
    }
    
    // If we've gotten a postal code add the appropriate params and sort
    if (FALSE === empty($postal_code)) {
      $postal_information = location_contact_get_postal_information($postal_code);

      if (FALSE !== $postal_information) {
        $this->query->set_param('sfield', $field);
        $this->query->set_param('pt', $postal_information->latitude . ',' . $postal_information->longitude);
        $this->query->add_sort('geodist()', $order, !empty($handler->options['exposed']));
      }
    }
  }
}
