<?php
class apache_solr_location_handler_field_distance extends views_handler_field_numeric {
  static $is_included = FALSE;
  
  function construct() {
    parent::construct();
    $this->definition['float'] = TRUE;
  }

  function query() {
    $this->field_alias = $this->query->add_solr_field($this->real_field);
  }

  /**
   * Overrides the render function to convert the lat/long to distance in miles
   * 
   * @see views_handler_field_numeric::render()
   * @return string
   */
  function render(&$values) {
    $value = $values->{$this->field_alias};
    $postal_code = $this->get_postal_code();

    foreach ($value as $id => $val) {
      $distance = NULL;

      if (NULL !== $postal_code && FALSE === empty($val)) {
        $point_values = explode(',', $val);

        if (1 < sizeof($point_values)) {
          $latitude = $point_values[0];
          $longitude = $point_values[1];

          $postal_information = location_contact_get_postal_information($postal_code);

          if (FALSE !== $postal_information) {
            $distance = $this->calculate_distance($latitude, $postal_information->latitude, $longitude, $postal_information->longitude);
            
            $value = $distance;
          }
        }
      }
    }

    $changed_values = new stdClass();
    $changed_values->{$this->field_alias} = $value;

    return parent::render($changed_values);
  }

  /**
   * Calculates the distance between two points in miles using the Great Circle formula
   * 
   * @param float $lat1
   * @param float $lat2
   * @param float $long1
   * @param float $long2
   * 
   * @return float
   */
  function calculate_distance($lat1, $lat2, $long1, $long2) {
    if (FALSE == $this->is_included) {
      module_load_include('php', 'apache_solr_location', 'includes/GeoCalc.class');
    }
    
    $oGC = new GeoCalc();

    return ConvKilometersToMiles($oGC->GCDistance($lat1, $long1, $lat2, $long2));
  }

  /**
   * Gets the postal code from either the argument or exposed filter
   * 
   * @return int
   */
  function get_postal_code() {
    $postal_code = NULL;

    $field = $this->real_field;
    $exposed_data = $this->view->exposed_data;

    // First try to get the postal code from the argument
    foreach ($this->view->argument as $key => $argument) {
      if ($field === $key) {
        $postal_code = $argument->get_value();
        break;
      }
    }

    // If that fails but we have an exposed filter use that
    if (TRUE === empty($postal_code) && FALSE === empty($exposed_data[$field])) {
      $postal_code = $exposed_data[$field];
    }

    return $postal_code;
  }
}
