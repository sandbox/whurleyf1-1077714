<?php

class apache_solr_location_handler_filter_postal_code extends views_handler_filter {
  /**
   * override query().
   */
  function query() {
    $this->value = array_filter($this->value);
    if (FALSE === empty($this->value)) {
      $postal_information = $this->get_postal_information($this->value[0]);
      if (FALSE != $postal_information) {
        $this->query->add_filter('!geofilt', array(
        	'pt' => $postal_information->latitude . ',' . $postal_information->longitude, 
        	'sfield' => 'loclatlong', 
        	'd' => $this->options['distance']));
      }
    }
  }

  function get_postal_information($postal_code) {
    $result = db_query("SELECT * FROM {zips} WHERE zip_code = '%s'", $postal_code);
    return db_fetch_object($result);
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['value'] = array(
      'contains' => array(
        'postalcode' => array('default' => ''),
    ),
    );

    $options['distance'] = array('default' => '25');
    $options['default_geoip'] = array('default' => '0');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['distance'] = array(
      '#type' => 'textfield',
      '#title' => t('Distance'),
      '#default_value' => $this->options['distance'],
    );

    $form['default_geoip'] = array(
      '#type' => 'radios',
      '#title' => t('Default to GeoIP'),
      '#default_value' => $this->options['default_geoip'],
      '#options' => array('0' => t('No'), '1' => t('Yes')),
    );
  }

  function value_form(&$form, &$form_state) {
    $form['value']['postalcode'] = array(
        '#type' => 'textfield',
        '#title' => t('Postal Code'),
        '#size' => 30,
        '#default_value' => $this->value['postalcode'],
    );
  }

  function exposed_form(&$form, &$form_state) {
    if (isset($this->options['expose']['identifier'])) {
      $key = $this->options['expose']['identifier'];

      // Default to geoip zipcode if user hasn't specified a zip code or explicitly specified no zipcode (by clearing field and clicking search)
      if (TRUE === empty($form_state['input'][$key]) && TRUE == $this->options['default_geoip'] && !isset($_GET['loclatlong'])) {
        $form_state['input'][$key] = $this->_get_geoip();
      }
      
      $form[$key] = array(
        '#type' => 'textfield',
        '#size' => 15,
        '#default_value' => $this->value,
        '#attributes' => array('title' => t('Postal Code')),
      );
    }
  }

  /**
   * Gets the current postal code from MaxMind
   * 
   * @return int
   */
  function _get_geoip() {
    $postal_code = NULL;

    if (TRUE === function_exists('apache_note')) {
      $postal_code = (int) apache_note('GEOIP_POSTAL_CODE');
    }

    return $postal_code;
  }
}
