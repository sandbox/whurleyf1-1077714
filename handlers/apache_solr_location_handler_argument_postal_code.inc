<?php
class apache_solr_location_handler_argument_postal_code extends apachesolr_views_handler_argument {

  function option_definition() {
    $options = parent::option_definition();

    $options['distance'] = array('default' => '25');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['distance'] = array(
      '#type' => 'textfield',
      '#title' => t('Distance'),
      '#default_value' => $this->options['distance'],
    );
  }

  function query() {
    if (!empty($this->options['break_phrase'])) {
      $this->value = explode(',', $this->argument);
    }
    else {
      $this->value = array($this->argument);
    }

    foreach ($this->value as $postal_code) {
      $postal_information = location_contact_get_postal_information($postal_code);

      if (FALSE != $postal_information) {
        $this->query->add_filter('!geofilt', array(
        	'pt' => $postal_information->latitude . ',' . $postal_information->longitude, 
        	'sfield' => 'loclatlong', 
        	'd' => $this->options['distance']));
      }
    }
  }
}
