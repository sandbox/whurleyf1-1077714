<?php
/**
 * Implementation of hook_views_handlers
 *
 * @return array
 */
function apache_solr_location_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'apache_solr_location') . '/handlers',
  ),
    'handlers' => array(
      'apache_solr_location_handler_filter_postal_code' => array(
        'parent' => 'views_handler_filter_numeric',
  ),
      'apache_solr_location_handler_argument_postal_code' => array(
        'parent' => 'apachesolr_views_handler_argument',
  ),
      'apache_solr_location_handler_sort_distance' => array(
        'parent' => 'apachesolr_views_handler_sort',
  ),
      'apache_solr_location_handler_field_distance' => array(
        'parent' => 'views_handler_field_numeric',
      ),
  ),
  );
}

/**
 * Implementation of hook_views_plugins().
 */
function apache_solr_location_views_plugins() {
  return array(
    'argument default' => array(
      'apache_solr_location_default_postal_code' => array(
        'title' => t('Current user Postal Code'),
        'handler' => 'apache_solr_location_default_postal_code',
        'path' => drupal_get_path('module', 'apache_solr_location') . '/plugins',
        'parent' => 'fixed', // so that the parent class is included
  ),
  ),
  );
}

function apache_solr_location_views_data_alter(&$data) {
  $data['apachesolr_node']['loclatlong'] = array(
     'title' => t('Location: Postal Code'),
     'help' => 'Apache Solr Location',
      'filter' => array(
         'handler' => 'apache_solr_location_handler_filter_postal_code',
  ),
      'argument' => array(
        'handler' => 'apache_solr_location_handler_argument_postal_code',
  ),
      'sort' => array(
        'handler' => 'apache_solr_location_handler_sort_distance',
  ),
      'field' => array(
        'handler' => 'apache_solr_location_handler_field_distance',
        'title' => t('Location: Distance from Search'),
        'help' => 'Location from the current filter / argument',
        'click sortable' => FALSE,
  ),
  );
}
